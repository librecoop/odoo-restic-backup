FROM golang:1.19-alpine AS builder

WORKDIR /go/src/github.com/restic/

ARG RESTIC_REPO=https://github.com/restic/restic/
ARG RESTIC_VERSION=v0.15.0

RUN apk --no-cache add git
RUN git clone ${RESTIC_REPO}
WORKDIR /go/src/github.com/restic/restic
RUN git checkout ${RESTIC_VERSION}

RUN go run build.go

FROM alpine:latest AS restic

RUN apk add --update --no-cache ca-certificates fuse openssh-client tzdata

WORKDIR /
COPY --from=builder /go/src/github.com/restic/restic/restic /usr/bin
COPY crontab /etc/crontabs/root
COPY entrypoint.sh /bin/entrypoint.sh
COPY lib.sh /usr/share/backup/lib.sh
COPY restore /bin/backup/restore
COPY backup /bin/backup/backup
ENV PATH ${PATH}:/bin/backup
RUN chmod +x /bin/entrypoint.sh

RUN apk --no-cache add curl bash

ENTRYPOINT ["/bin/entrypoint.sh"]
