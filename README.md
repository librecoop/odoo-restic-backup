# Restic Odoo Backups

Docker container to automate the backup of an odoo server with restic into S3 compatible remote storage 

## Variables
- ODOO_URL: Url of the odoo instance, if the odoo instance is in the same docker network the service name can be used. (Default: http://odoo:8069)
- ODOO_DATABASE: Name of the odoo database to backup. (Default: odoo)
- ODOO_ADMIN_PASSWORD: Admin pasword of the odoo database manager. If no password is set, the backup will not work (Default: none) 
- REPO_URL: Url of the object storage. (Required). For Backblaze over S3 this is ${S3_ENDPOINT}/${BUCKET_NAME}
- REPO_NAME: Name of the restic repository where the backup will be stored (Default: odoo)
- RESTIC_PASSWORD: The password of the restic repository
- FS_REPO: Filesystem repo that can be used if S3/B2 variables are not set (Default: /var/lib/restic/repo)
- AWS_ACCESS_KEY_ID: Access id of the S3 storage account. Same as B2_ACCOUNT_ID for B2
- AWS_SECRET_ACCESS_KEY: Secret for the S3 storage account. Same as B2_ACCOUNT_KEY for B2.
- KEEP_POLICY: Restic policy to forget old snapshots. (For example: "--keep-within-daily 14d --keep-within-weekly 1m --keep-within-monthly 1y --keep-within-yearly 75y
- RESTORE_SNAPSHOT: If set to a valid snapshot (or "latest"), it will be restored into Odoo.
")

## Compatible storage
- Backblaze (using S3's API)
- Filesystem

