#!/bin/bash

RESTIC_PASSWORD_FILE=/tmp/restic_pass

log() {
   local lvl=$1
   local msg=${@: 2}

    >&2 echo "$(date +'%Y-%m-%dT%H:%M:%S%z') | ${lvl} | ${msg//$'\n'/\\n}"
}

fatal() {
    log FATAL $@
    exit 1
}

error() {
    log ERROR $@
}

warn() {
    log WARN $@
}

info() {
    log INFO $@
}

# ENV Variables required:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - REPO
do_restic() {
    local args=$@
    restic \
        --verbose \
        --password-file ${RESTIC_PASSWORD_FILE} \
        --repo ${REPO} \
        ${args}
}

init() {
    # Check required variables are set
    if [[ -z ${ODOO_ADMIN_PASSWORD} ]]; then
        error "Missing ODOO_ADMIN_PASSWORD, exiting process..."
    fi
    if [[ -z ${RESTIC_PASSWORD} ]]; then
        error "Missing RESTIC_PASSWORD, exiting process..."
    fi

    echo ${RESTIC_PASSWORD} > ${RESTIC_PASSWORD_FILE}
    chmod 600 ${RESTIC_PASSWORD_FILE}

    REPO=s3:${REPO_URL}/${REPO_NAME}

    if [ -z ${REPO_URL} ] || [ -z ${REPO_NAME} ] ||  [ -z ${AWS_SECRET_ACCESS_KEY} ] || [ -z ${AWS_ACCESS_KEY_ID} ]; then
        warn "Missing B2 / S3 information. Falling back to filesystem repository."
        REPO=${FS_REPO:-/var/lib/restic/repo}
    fi

    info "Backup repository: ${REPO}"

    do_restic check || do_restic init

    info "Backup repository: ${REPO}"
}
