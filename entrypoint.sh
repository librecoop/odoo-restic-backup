#!/bin/bash
set -eo pipefail

SHARE_DIR=/usr/share/backup/
source ${SHARE_DIR}/lib.sh

init

interrupt() {
    info "Trapped SIGTERM. Exiting"
    exit 0
}
trap interrupt SIGTERM

case "${1:-cron}" in
     cron)
        LOG_FILE="/var/log/daily-odoo-backup.log"
        [ -f ${LOG_FILE} ] || touch ${LOG_FILE}
        crond -f -d 8 &
        tail -f ${LOG_FILE} &
        wait
        ;;
    * )
        exec $@
        ;;
esac
